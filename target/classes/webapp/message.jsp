<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>

<%

String message = (String)request.getSession().getAttribute("message");
String MainFolder = System.getProperty("main.folder");
String Lang = request.getServletPath().split("/")[1];
request.getSession().setAttribute("lang", Lang);
%>
<c:set var="lang" value="<%=Lang %>" />
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form - Modal</title>
  
  <link rel="stylesheet" href="<%=MainFolder %>/css/reset.min.css">

  <link rel='stylesheet prefetch' href='<%=MainFolder %>/css/googleapis.css'>

      <link rel="stylesheet" href="<%=MainFolder %>/css/style.css">

  
</head>

<body>
  
<!-- Form-->
<div class="form">
  <div class="form-toggle"></div>
  <div class="form-panel one">
    <div class="form-header">
      <h1>Dear customer</h1>
    </div>
	    <div class="form-header">
	    <% if (message != null) {%>
	<h3 class="message"><%=message %></h3>
	<%} else response.sendRedirect("home"); %>
	    </div>
    <div class="form-content">
      <form action="subscriber_profile" method="GET" >
        
        <div class="form-group">
          <button type="submit">Continue</button>
        </div>
      </form>
    </div>
  </div>
 
</div>

  <script src='<%=MainFolder %>/js/jquery.min.js'></script>
<script src='<%=MainFolder %>/js/vLmRVp.js'></script>

    <script src="<%=MainFolder %>/js/index.js"></script>

</body>
</html>
