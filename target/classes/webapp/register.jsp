<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String MainFolder = System.getProperty("main.folder");
String Lang = request.getServletPath().split("/")[1];
%>
<c:set var="lang" value="<%=Lang %>" />
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Registration</title>
  
  <link rel="stylesheet" href="<%=MainFolder %>/css/reset.min.css">

  <link rel='stylesheet prefetch' href='<%=MainFolder %>/css/googleapis.css'>

      <link rel="stylesheet" href="<%=MainFolder %>/css/style.css">

  
</head>

<body>
  
<!-- Form-->
<div class="form">
  <div class="form-toggle visible"></div>
  <div class="form-panel one active">
    <div class="form-header">
      <h1><%=Localizator.execute("form.reg.header", Lang) %></h1>
    </div >
    <div class="form-content">
      <form action="PublicAction" method="POST" enctype="application/x-www-form-urlencoded">
        <input type="hidden" id="action" name="action" value="register">
        <div class="form-group">
          <label for="username"><%=Localizator.execute("form.username", Lang) %></label>
          <input type="text" id="username" name="username" required="required" pattern="^[a-zA-Z0-9&!#?\.]{5,16}$" oninput="ValidateLogin(this);">
        </div>
        <div class="form-group">
          <label for="email"><%=Localizator.execute("form.email", Lang) %></label>
          <input type="email" id="email" name="email" required="required" pattern="(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)" oninput="ValidateEmail(this);">
        </div><div class="form-group">
          <label for="phone"><%=Localizator.execute("form.phone", Lang) %></label>
          <input type="phone" id="phone" name="phone" required="required" pattern="^\+375[0-9]{9}$"
		oninput="ValidatePhone(this);">
        </div><div class="form-group">
          <label for="password"><%=Localizator.execute("form.password", Lang) %></label>
          <input type="password" id="password" name="password" required="required" pattern="^.{10,20}$"
		oninput="ValidatePassword1(this);">
        </div>
        <div class="form-group">
          <label for="cpassword"><%=Localizator.execute("form.confirm.password", Lang) %></label>
          <input type="password" id="cpassword" name="cpassword" required="required" oninput="ValidatePassword1(this);">
        </div>  
      <div class="form-group">
          <button type="submit" onclick="Validate()"><%=Localizator.execute("form.register", Lang) %></button>
        </div></form>
    </div>
  </div>
</div>

  <script src='<%=MainFolder %>/js/jquery.min.js'></script>


    <script src="<%=MainFolder %>/js/index.js"></script>

</body>
</html>
