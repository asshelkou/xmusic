<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="by.epam.andreyshelkov.xmusic.entities.AbstractUser"%>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>
<%@ taglib uri="/WEB-INF/CategoryDisplayer.tld" prefix="cat"%>
<%@ taglib uri="/WEB-INF/SongsDisplayer.tld" prefix="songs"%>

<%
	AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
	String Status = "";
	String ChStateButton1 = "";
	String ChStateButton2 = "";
	String Lang = request.getServletPath().split("/")[1];
	String MainFolder = System.getProperty("main.folder");
	
%>

<c:set var="page" value="${param.page-1}" />
<c:if test="${param.page == null}">
	<c:set var="page" value="0" />
</c:if>
<c:if test="${param.page < 1}">
	<c:set var="page" value="0" />
</c:if>

<c:if test="${param.category == null}">
	<sql:query var="rs" dataSource="jdbc/xmusic">
select s.id, s.name as name, s.link as link, p.price as price from xmusic.song s 
    join xmusic.priceplan p on (s.price_plan_id = p.id)
  where status_id = 3;
</sql:query>

	<sql:query var="p_count" dataSource="jdbc/xmusic">
select (count(*) DIV 12)+1 as count from xmusic.song where status_id = 3;
</sql:query>

	<c:forEach var="row" items="${p_count.rows}">
		<c:set var="pages_count" value="${row.count}" />
	</c:forEach>
	<c:set var="category_param" value="" />
</c:if>

<c:if test="${param.category != null}">
	<sql:query var="rs" dataSource="jdbc/xmusic">
select s.id, s.name as name, s.link as link, p.price as price from xmusic.song s 
    join xmusic.priceplan p on (s.price_plan_id = p.id)
  where status_id = 3 and s.category_id = '${param.category}';
</sql:query>

	<sql:query var="p_count" dataSource="jdbc/xmusic">
select (count(*) DIV 12)+1 as count from xmusic.song where status_id = 3 and category_id = '${param.category}';
</sql:query>

	<c:forEach var="row" items="${p_count.rows}">
		<c:set var="pages_count" value="${row.count}" />
	</c:forEach>

	<c:set var="category_param" value="category=${param.category}&" />
</c:if>





<cat:CategoryDisplayer />

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title><%=Localizator.execute("header.title", Lang) %></title>

<!-- Bootstrap core CSS -->
<link href="<%=MainFolder %>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=MainFolder %>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet"
	type="<%=MainFolder %>/text/css">
<!-- Custom styles for this template -->
<link href="<%=MainFolder %>/css/shop-homepage.css" rel="stylesheet">

<script>
	function doPlay(e) {
		if(document.getElementById('player').src != e.value) {
		document.getElementById('player').src = e.value;
		}
		document.getElementById('player').play();
	}

	function doPause() {
		document.getElementById('player').pause();
	}

	function doStop() {
		document.getElementById('player').pause();
		document.getElementById('player').currentTime = 0;
	}
</script>

</head>

<body>

	<audio id="player" src=""></audio>

	<nav
		class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
		<div class="container">
			<a class="navbar-brand" href="home">X-Music</a>

			<!-- Topbar Navbar -->
			<ul class="navbar-nav">

				<%
					if (User != null) {
				%>

				<!-- Nav Item - User Information -->
				<li class="nav-item dropdown no-arrow"><a
					class="nav-link dropdown-toggle" href="#" id="userDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <span
						class="mr-2 d-none d-lg-inline text-gray-600 small"><%=User.getLogin()%></span>
				</a> <!-- Dropdown - User Information -->
					<div
						class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
						aria-labelledby="userDropdown" style="z-index: 99999;">

						<a class="dropdown-item" href="subscriber_profile"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i><%=Localizator.execute("header.menu.profile", Lang) %>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="logout"> <i
							class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							<%=Localizator.execute("header.menu.logout", Lang) %>
						</a>
					</div></li>

				<%
					} else {
				%>

				<li class="nav-item dropdown no-arrow"><a
					class="nav-link dropdown-toggle" href="#" id="userDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <span
						class="mr-2 d-none d-lg-inline text-gray-600 small"><%=Localizator.execute("header.menu.login", Lang) %></span>
				</a> <!-- Dropdown - User Information -->
					<div
						class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
						aria-labelledby="userDropdown" style="z-index: 99999;">
						<a class="dropdown-item" href="authorize"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i><%=Localizator.execute("header.menu.authorize", Lang) %>
						</a> <a class="dropdown-item" href="register"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i><%=Localizator.execute("header.menu.registration", Lang) %>
						</a>
					</div></li>
				<%
					}
				%>
			</ul>
		</div>
	</nav>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link"
						href="home"><%=Localizator.execute("header.menu.home", Lang) %> <span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="about"><%=Localizator.execute("header.menu.about", Lang) %></a></li>
					<li class="nav-item"><a class="nav-link" href="faq"><%=Localizator.execute("header.menu.faq", Lang) %></a>
					</li>
					<li class="nav-item"><a class="nav-link" href="contact"><%=Localizator.execute("header.menu.contact", Lang) %></a>
					</li>
				</ul>
				<!-- Topbar Search -->

			</div>
			<form
				class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
				<div class="input-group">
					<input type="text" class="form-control bg-light border-0 small"
						placeholder="<%=Localizator.execute("header.menu.search", Lang) %>" aria-label="Search"
						aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button class="btn btn-primary" type="button">
							<i class="fas fa-search fa-sm"></i>
						</button>
					</div>
				</div>
			</form>
		</div>

	</nav>

	<!-- Page Content -->
	<main class="navbar navbar-expand-lg navbar-dark fixed-top">
	<div class="container">

		<div class="row">

			<div class="col-lg-3">

				<h1 class="my-4"><%=Localizator.execute("header.categories", Lang) %></h1>
				<div class="list-group">

<c:set var="lang" value="<%=Lang %>" />
<c:choose>
    <c:when test="${lang == en}">
 					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="?category=${row.getId()}"
							class="list-group-item">${row.getNameEn()}</a>
					</c:forEach>
    </c:when>    
    <c:otherwise>
					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="?category=${row.getId()}"
							class="list-group-item">${row.getName()}</a>
					</c:forEach>
    </c:otherwise>
</c:choose>
				</div>

			</div>
			<!-- /.col-lg-3 -->

			<div class="col-lg-9">

				<div id="carouselExampleIndicators" class="carousel slide my-4"
					data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0"
							class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img class="d-block img-fluid" src="<%=MainFolder %>/images/7xS8M76kmIM.jpg"
								alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="<%=MainFolder %>/images/9tDKeIwuQFg.png"
								alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="<%=MainFolder %>/images/kARMpoUolFs.png"
								alt="Third slide">
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators"
						role="button" data-slide="prev"> <span
						class="carousel-control-prev-icon" aria-hidden="true"></span> <span
						class="sr-only">Previous</span>
					</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
						role="button" data-slide="next"> <span
						class="carousel-control-next-icon" aria-hidden="true"></span> <span
						class="sr-only">Next</span>
					</a>
				</div>

				<div class="row">

					<c:forEach var="row" items="${rs.rows}" begin="${page*12}"
						end="${page*12+12-1}">
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="card h-100">
								<%
									if (User != null) {
								%>
								<form action="<%=MainFolder %>/UserAction" method="POST"
									enctype="application/x-www-form-urlencoded" class="buy-btn">
									<input type="hidden" class="buy-btn" name="songId"
										value="${row.id}" /> <input type="submit" class="buy-btn"
										name="action" value="Buy" />
								</form>
								<%
									}
								%>
								<div class="card-body">
									<h4 class="card-title">
										<a href="#"></a>
									</h4>
									<h5>${row.price}BYN</h5>
									<p class="card-text">${row.name}</p>
								</div>
								<div class="card-footer">

									<label> <img src="<%=MainFolder %>/images/play.png"> <input
										class="player-ctrl" type="submit" onclick="doPlay(this)"
										value="${row.link}"></input>
									</label> <label> <img src="<%=MainFolder %>/images/pause.png"> <input
										class="player-ctrl" type="submit"
										onclick="document.getElementById('player').pause()"></input>
									</label> <label> <img src="<%=MainFolder %>/images/stop.png"> <input
										class="player-ctrl" type="submit"
										onclick="document.getElementById('player').pause();document.getElementById('player').currentTime = 0;"></input>
									</label>
								</div>
							</div>
						</div>
					</c:forEach>


				</div>
				<!-- /.row -->
				<div class="pages">

					<a class="page" href="?${category_param}page=1">
						<p class="page"><%=Localizator.execute("home.main.page.first", Lang) %></p> <c:if test="${page != 0}">
					</a> <a class="page" href="?${category_param}page=${page}">
						<p class="page"><%=Localizator.execute("home.main.page.previous", Lang) %></p>
					</a>
					</c:if>
					<a class="page" href="?${category_param}page=${page+1}">
						<p class="page">
							<c:out value="${page+1}"></c:out>
						</p>
					</a>
					<c:if test="${page != pages_count-1}">
						<a class="page" href="?${category_param}page=${page+2}">
							<p class="page"><%=Localizator.execute("home.main.page.next", Lang) %></p>
						</a>
					</c:if>
					<a class="page" href="?${category_param}page=${pages_count}">
						<p class="page"><%=Localizator.execute("home.main.page.last", Lang) %></p>
					</a>
				</div>

			</div>
			<!-- /.col-lg-9 -->


		</div>
		<!-- /.row -->

	</div>
	</main>
	<!-- /.container -->

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Your
				Website 2019</p>
		</div>
		<!-- /.container -->
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="<%=MainFolder %>/vendor/jquery/jquery.min.js"></script>
	<script src="<%=MainFolder %>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
