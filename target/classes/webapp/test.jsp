<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/CategoryDisplayer.tld" prefix="cat"%>
<%@ taglib uri="/WEB-INF/SongsDisplayer.tld" prefix="songs"%>
<%@ taglib uri="/WEB-INF/UserSongsDisplayer.tld" prefix="us"%>



<html>
<head>
<title>DB Test</title>
</head>
<body>

	<h2>Results</h2>
	
<cat:CategoryDisplayer />

<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>' begin="0" end="10">
   ${row.getId()}<br/>
   ${row.getName()}<br/>
   ${row.getNameEn()}<br/>
</c:forEach>

<songs:SongsDisplayer category="${param.category}" statuses="3"/>

<c:forEach var="row" items='<%=pageContext.getAttribute("songs") %>' begin="0" end="10">
   ${row.getId()}<br/>
   ${row.getName()}<br/>
</c:forEach>
<br/><br/><br/><br/><br/><br/><br/>

<us:UserSongsDisplayer userId="2"/>
<c:forEach var="row" items='<%=pageContext.getAttribute("user_songs") %>' begin="0" end="10">
   ${row.getId()}<br/>
   ${row.getName()}<br/>
</c:forEach>

</body>
</html>