<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
String MainFolder = System.getProperty("main.folder");
%>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form - Modal</title>
  
  <link rel="stylesheet" href="<%=MainFolder %>/css/reset.min.css">

  <link rel='stylesheet prefetch' href='<%=MainFolder %>/googleapis.css'>

      <link rel="stylesheet" href="<%=MainFolder %>/css/style.css">

  
</head>

<body>
  
<!-- Form-->
<div class="form">
  <div class="form-toggle"></div>
  <div class="form-panel one">
    <div class="form-header">
      <h1>Dear customer</h1>
    </div>
	    <div class="form-header">
	    
	<h3 class="message">You entered wrong username or password. Please try again.</h3>

	    </div>
    <div class="form-content">
      <form action="subscriber_profile.jsp" method="GET" >
        
        <div class="form-group">
          <button type="submit">Continue</button>
        </div>
      </form>
    </div>
  </div>
 
</div>

  <script src='<%=MainFolder %>/js/jquery.min.js'></script>
<script src='<%=MainFolder %>/js/vLmRVp.js'></script>

    <script src="<%=MainFolder %>/js/index.js"></script>

</body>
</html>
