<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>
<%@ page import="by.epam.andreyshelkov.xmusic.entities.AbstractUser"%>

<%
	AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
	String Status = "";
	String ChStateButton1 = "";
	String ChStateButton2 = "";
	String MainFolder = System.getProperty("main.folder");
	String Lang = (String)request.getSession().getAttribute("lang");
%>
<%
	if (User == null) {
%>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title><%=Localizator.execute("form.login.header", Lang) %></title>
  
  <link rel="stylesheet" href="<%=MainFolder %>/css/reset.min.css">

  <link rel='stylesheet prefetch' href='<%=MainFolder %>/css/googleapis.css'>

      <link rel="stylesheet" href="<%=MainFolder %>/css/style.css">

  
</head>

<body>
  
<!-- Form-->
<div class="form">
  <div class="form-toggle"></div>
  <div class="form-panel one">
    <div class="form-header">
      <h1><%=Localizator.execute("form.login.header", Lang) %></h1>
    </div>
    <div class="form-content">
      <form action="j_security_check" method="POST" >
        <div class="form-group">
          <label for="username"><%=Localizator.execute("form.username", Lang) %></label>
          <input type="text" id="j_username" name="j_username" required="required"/>
        </div>
        <div class="form-group">
          <label for="password"><%=Localizator.execute("form.password", Lang) %></label>
          <input type="password" id="j_password" name="j_password" required="required"/>
        </div>
        <div class="form-group">
        <a class="form-recovery" href="forgot_password"><%=Localizator.execute("forgot.password", Lang) %></a>
        </div>
        <div class="form-group">
          <button type="submit"><%=Localizator.execute("form.login", Lang) %></button>
        </div>
      </form>
    </div>
  </div>
</div>

  <script src='<%=MainFolder %>/js/jquery.min.js'></script>


    <script src="<%=MainFolder %>/js/index.js"></script>

</body>
</html>
<%
	} else {
		response.sendRedirect("home");	
	}
%>
