<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import="by.epam.andreyshelkov.xmusic.entities.AbstractUser" %>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>
<%@ taglib uri="/WEB-INF/CategoryDisplayer.tld" prefix="cat"%>

<%

AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
String Lang = request.getServletPath().split("/")[1];
String MainFolder = System.getProperty("main.folder");
request.getSession().setAttribute("lang", Lang);
%>

<c:set var="lang" value="<%=Lang %>" />
<cat:CategoryDisplayer />

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title><%=Localizator.execute("header.title", Lang) %></title>

<!-- Bootstrap core CSS -->
<link href="<%=MainFolder %>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=MainFolder %>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet"
	type="text/css">
<!-- Custom styles for this template -->
<link href="<%=MainFolder %>/css/shop-homepage.css" rel="stylesheet">

</head>

<body>


	<nav
		class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
		<div class="container">
			<a class="navbar-brand" href="home">X-Music</a>

			<!-- Topbar Navbar -->
			<ul class="navbar-nav">

<% if (User != null){%>

				<!-- Nav Item - User Information -->
				<li class="nav-item dropdown no-arrow">
				<a class="nav-link dropdown-toggle" href="#" id="userDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <span
						class="mr-2 d-none d-lg-inline text-gray-600 small"><%=User.getLogin() %></span>
				</a> <!-- Dropdown - User Information -->
					<div
						class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
						aria-labelledby="userDropdown" style="z-index: 99999;">
						
						<a class="dropdown-item" href="subscriber_profile"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i> <%=Localizator.execute("header.menu.profile", Lang) %>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="logout" > <i
							class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							<%=Localizator.execute("header.menu.logout", Lang) %>
						</a>
					</div></li>
					
				<% } else {%>	
				
								<li class="nav-item dropdown no-arrow"><a
					class="nav-link dropdown-toggle" href="#" id="userDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <span
						class="mr-2 d-none d-lg-inline text-gray-600 small"><%=Localizator.execute("header.menu.login", Lang) %></span>
				</a> <!-- Dropdown - User Information -->
					<div
						class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
						aria-labelledby="userDropdown" style="z-index: 99999;">
						<a class="dropdown-item" href="authorize"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i><%=Localizator.execute("header.menu.authorize", Lang) %>
						</a>
						<a class="dropdown-item" href="register"> <i
							class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i><%=Localizator.execute("header.menu.registration", Lang) %>
						</a>
					</div></li>
<% } %>	
			</ul>
		</div>
	</nav>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="home"><%=Localizator.execute("header.menu.home", Lang) %>
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="about"><%=Localizator.execute("header.menu.about", Lang) %></a></li>
					<li class="nav-item"><a class="nav-link" href="faq"><%=Localizator.execute("header.menu.faq", Lang) %></a>
					</li>
					<li class="nav-item"><a class="nav-link" href="contact"><%=Localizator.execute("header.menu.contact", Lang) %></a>
					</li>
				</ul>
				<!-- Topbar Search -->

			</div>
			<form
				class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
				<div class="input-group">
					<input type="text" class="form-control bg-light border-0 small"
						placeholder="<%=Localizator.execute("header.menu.search", Lang) %>" aria-label="Search"
						aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button class="btn btn-primary" type="button">
							<i class="fas fa-search fa-sm"></i>
						</button>
					</div>
				</div>
			</form>
		</div>

	</nav>

	<!-- Page Content -->
	<main class="navbar navbar-expand-lg navbar-dark fixed-top">
	<div class="container">

		<div class="row">

			<div class="col-lg-3">

				<h1 class="my-4"><%=Localizator.execute("header.categories", Lang) %></h1>
				<div class="list-group">

<c:choose>
    <c:when test="${lang == 'en'}">
 					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="home?category=${row.getId()}"
							class="list-group-item">${row.getNameEn()}</a>
					</c:forEach>
    </c:when>    
    <c:otherwise>
					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="home?category=${row.getId()}"
							class="list-group-item">${row.getName()}</a>
					</c:forEach>
    </c:otherwise>
</c:choose>
				</div>

			</div>
			<!-- /.col-lg-3 -->

			<div class="col-lg-9">

				<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
					<p><%=Localizator.execute("contact.body.1", Lang) %></p>
					<p><b><%=Localizator.execute("contact.body.2", Lang) %></b></p>
					<p><%=Localizator.execute("contact.body.3", Lang) %></p>
					<p><%=Localizator.execute("contact.body.4", Lang) %></p>
					<div class="carousel-inner" role="listbox" style="height: 0;">   						
						<div class="carousel-item active" style="height: 0;">
							<img class="d-block img-fluid" alt="First slide" src="<%=MainFolder %>/images/900x350.png">
						</div>				
				    </div>
					 
				</div>

			

			</div>
			<!-- /.col-lg-9 -->


		</div>
		<!-- /.row -->

	</div>
	</main>
	<!-- /.container -->

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Your
				Website 2019</p>
		</div>
		<!-- /.container -->
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="<%=MainFolder %>/vendor/jquery/jquery.min.js"></script>
	<script src="<%=MainFolder %>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
