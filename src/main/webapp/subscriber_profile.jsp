<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/CategoryDisplayer.tld" prefix="cat"%>
<%@ taglib uri="/WEB-INF/UserSongsDisplayer.tld" prefix="us"%>
<%@ page import="by.epam.andreyshelkov.xmusic.utils.Localizator"%>
    
<%@ page import="by.epam.andreyshelkov.xmusic.entities.AbstractUser" %>

<%

AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
String Status = "";
String ChStateButton1 = "";
String ChStateButton2 = "";
String ActionButton1 = "";
String ActionButton2 = "";
String MainFolder = System.getProperty("main.folder");
String Lang = "";
if (request.getServletPath() != null){
	Lang = request.getServletPath().split("/")[1];
}
request.getSession().setAttribute("lang", Lang);
%>

<cat:CategoryDisplayer />            

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><%=Localizator.execute("header.title", Lang) %></title>

  <!-- Bootstrap core CSS -->
  <link href="<%=MainFolder %>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<%=MainFolder %>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template -->
  <link href="<%=MainFolder %>/css/shop-homepage.css" rel="stylesheet">
<script>
	function doPlay(e) {
		if(document.getElementById('player').src != e.value) {
		document.getElementById('player').src = e.value;
		}
		document.getElementById('player').play();
	}

	function doPause() {
		document.getElementById('player').pause();
	}

	function doStop() {
		document.getElementById('player').pause();
		document.getElementById('player').currentTime = 0;
	}
</script>
</head>

<body>
<% if (User != null){%>

<audio id="player" src=""></audio>
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <div class="container">
      <a class="navbar-brand" href="#">Start Bootstrap</a>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><%=User.getLogin() %></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown" style="z-index:99999;">
                <a class="dropdown-item" href="subscriber_profile">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  <%=Localizator.execute("header.menu.profile", Lang) %>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  <%=Localizator.execute("header.menu.logout", Lang) %>
                </a>
              </div>
            </li>

			</ul>
			</div>
        </nav>
		  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="home"><%=Localizator.execute("header.menu.home", Lang) %>
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="about"><%=Localizator.execute("header.menu.about", Lang) %></a></li>
					<li class="nav-item"><a class="nav-link" href="faq"><%=Localizator.execute("header.menu.faq", Lang) %></a>
					</li>
					<li class="nav-item"><a class="nav-link" href="contact"><%=Localizator.execute("header.menu.contact", Lang) %></a>
					</li>
				</ul>
		          <!-- Topbar Search -->

      </div>
	  	<form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="<%=Localizator.execute("header.menu.search", Lang) %>" aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
    </div>

  </nav>

  <!-- Page Content -->
   <main class="navbar navbar-expand-lg navbar-dark fixed-top">
  <div class="container">

    <div class="row">

			<div class="col-lg-3">

				<h1 class="my-4"><%=Localizator.execute("header.categories", Lang) %></h1>
				<div class="list-group">
<c:choose>
    <c:when test="${lang == 'en'}">
 					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="?category=${row.getId()}"
							class="list-group-item">${row.getNameEn()}</a>
					</c:forEach>
    </c:when>    
    <c:otherwise>
					<c:forEach var="row" items='<%=pageContext.getAttribute("categories") %>'>
						<a href="?category=${row.getId()}"
							class="list-group-item">${row.getName()}</a>
					</c:forEach>
    </c:otherwise>
</c:choose>
				</div>

			</div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9">

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
     <div class="profile">
    
    <div class="card h-100">
              
              <div class="card-body">

                <h5><%=Localizator.execute("user.profile.username", Lang) %>: <%=User.getLogin() %> </h5>
                <h5><%=Localizator.execute("user.profile.email", Lang) %>: <%=User.getEmail() %> </h5>
                
              <h5><%=Localizator.execute("user.profile.phone", Lang) %>: <%=User.getPhone() %></h5>
              <% switch (User.getStatusId()) {
              case 1: 
            	  Status = Localizator.execute("user.profile.activated", Lang); 
            	  ChStateButton1 = Localizator.execute("user.profile.pause", Lang); 
            	  ChStateButton2 = Localizator.execute("user.profile.deactivate", Lang); 
            	  ActionButton1 = "Pause";
            	  ActionButton2 = "Deactivate";
            	  break;
              case 2: 
            	  Status = Localizator.execute("user.profile.onpause", Lang); ; 
            	  ChStateButton1 = Localizator.execute("user.profile.activate", Lang); 
            	  ChStateButton2 = Localizator.execute("user.profile.deactivate", Lang); 
            	  ActionButton1 = "Activate";
            	  ActionButton2 = "Deactivate";
            	  break;
              case 3: 
            	  Status = Localizator.execute("user.profile.deactivated", Lang); 
            	  ChStateButton1 = Localizator.execute("user.profile.activate", Lang); 
            	  ChStateButton2 = Localizator.execute("user.profile.pause", Lang); 
            	  ActionButton1 = "Activate";
            	  ActionButton2 = "Pause";
            	  break;
              }
              %>
              <h5><%=Localizator.execute("user.profile.status", Lang) %>: <%=Status %></h5></div> 
<us:UserSongsDisplayer userId='<%=((Integer)User.getId()).toString() %>'/>
         
             <div class="ch-state-btn" > 
            <form action="UserAction" method="POST"enctype="application/x-www-form-urlencoded" class="ch-status">
            <input type="submit" name="description" value="<%=ChStateButton1 %>" class="buy-btn ch-status" />
            <input type="hidden" name="action" value="<%=ActionButton1 %>"/>
            </form>
            <form action="UserAction" method="POST"enctype="application/x-www-form-urlencoded" class="ch-status">
            <input type="submit" name="description" value="<%=ChStateButton2 %>" class="buy-btn ch-status" />
            <input type="hidden" name="action" value="<%=ActionButton2 %>" />
            </form>

    </div>
    </div>
<div class="song-list">
        
    <div class="card h-100">

<c:forEach var="song" items='<%=pageContext.getAttribute("user_songs") %>'>

              
              <div class="song-body">
                <p class="song-title">${song.getName()}</p>
                

              <div class="audio-ctrl">

              <label> <img src="<%=MainFolder %>/images/play.png"> <input
										class="player-ctrl" type="submit" onclick="doPlay(this)"
										value="${song.getLink()}"></input>
									</label> <label> <img src="<%=MainFolder %>/images/pause.png"> <input
										class="player-ctrl" type="submit"
										onclick="document.getElementById('player').pause()"></input>
									</label> <label> <img src="<%=MainFolder %>/images/stop.png"> <input
										class="player-ctrl" type="submit"
										onclick="document.getElementById('player').pause();document.getElementById('player').currentTime = 0;"></input>
									</label>
              <form action="UserAction" method="POST"enctype="application/x-www-form-urlencoded" class="plst-ctrl">  
              <input type="hidden" name="songId" value="${song.getId()}" />
              <input type="submit" name="description" value="<%=Localizator.execute("user.profile.drop", Lang) %>" class="buy-btn song-control" />
               <input type="hidden" name="action" value="Drop" />
              </form> 
              </a>
<form action="UserAction" method="POST"enctype="application/x-www-form-urlencoded" class="plst-ctrl">  
<c:if test="${song.isInPlayList()}">

              <input type="hidden" name="songId" value="${song.getId()}" />
	          <input type="submit" name="action" value="X" class="buy-btn rm-from-plst" />

</c:if>             
<c:if test="${!song.isInPlayList()}">

               <input type="hidden" name="songId" value="${song.getId()}">
			  <input type="submit" name="action" value="V" class="buy-btn add-to-plst" />
			
 </c:if>  
    </form>              
              </div></div>
</c:forEach>
              
            </div></div>
	</div>
        </div>





        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  </main>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Xmusic 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<%=MainFolder %>/vendor/jquery/jquery.min.js"></script>
  <script src="<%=MainFolder %>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 <% } else response.sendRedirect("home"); %>
</body>

</html>
