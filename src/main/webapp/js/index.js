function ValidateLogin(textbox) {


     if(textbox.validity.patternMismatch){
        textbox.setCustomValidity('Логин может содержать буквы латинского алфавита, цифры, символы &!#? и иметь длинну от 5 до 16 символов');
    } else if (textbox.validity.valueMissing) {
		textbox.setCustomValidity("Это поле не может быть пустым");
	} 
    else {
        textbox.setCustomValidity('');
    }
	
    return true;
}

function ValidateEmail(textbox) {

     if(textbox.validity.patternMismatch){
        textbox.setCustomValidity('Пожалуйста, введите, корректный Email');
    } else if (textbox.validity.valueMissing) {
		textbox.setCustomValidity("Это поле не может быть пустым");
	}   
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

function ValidatePhone(textbox) {

     if(textbox.validity.patternMismatch){
        textbox.setCustomValidity('Пожалуйста, введите номер телефона в формате +375XXXXXXXXX');
    } else if (textbox.validity.valueMissing) {
		textbox.setCustomValidity("Это поле не может быть пустым");
	}   
    else {
        textbox.setCustomValidity('');
    }
    return true;
}



function ValidatePassword1(textbox) {

     if(textbox.validity.patternMismatch){
        textbox.setCustomValidity('Пароль должен быть от 10 до 20 символов');
    } else if (textbox.validity.valueMissing) {
		textbox.setCustomValidity("Это поле не может быть пустым");
	}   
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

function ValidatePassword2(textbox) {
	var password1 = document.getElementsByTagName("input")[4];
	console.log(password1.value)
     if(textbox.value != password1.value){
        textbox.setCustomValidity('Пароли не совпадают');
    } else if (textbox.validity.valueMissing) {
		textbox.setCustomValidity("Это поле не может быть пустым");
	}   
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

function Validate() {
	var elements = document.getElementsByTagName("input");
	ValidateLogin(elements[1]);
	ValidateEmail(elements[2]);
	ValidatePhone(elements[3]);
	ValidatePassword1(elements[4]);
	ValidatePassword2(elements[5]);
	
}

