package by.epam.andreyshelkov.xmusic.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Category;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.entities.ContentFactory;
import by.epam.andreyshelkov.xmusic.entities.UserFactory;
import by.epam.andreyshelkov.xmusic.exceptions.ContentCreationException;
import by.epam.andreyshelkov.xmusic.exceptions.ContentDAOException;
import by.epam.andreyshelkov.xmusic.exceptions.UserCreationException;
import by.epam.andreyshelkov.xmusic.exceptions.UserDAOException;
import by.epam.andreyshelkov.xmusic.interfaces.ContentDAO;

public class MySqlContentDAO implements ContentDAO {
	
	private static ContentDAO instance;
	private static AtomicInteger instanceCount = new AtomicInteger(0);
	
	private String SqlStatementGetSongByUsername = "SELECT s.id, s.name, s.link, p.price FROM xmusic.song s JOIN xmusic.priceplan p ON (p.id = s.price_plan_id) WHERE s.name = ? LIMIT 1";
	private String SqlStatementGetCategories = "SELECT * FROM xmusic.songcategory WHERE id != 6;";

	private MySqlContentDAO() {
		instanceCount.getAndIncrement();
	}

	public static ContentDAO getInstance() {
		if (instanceCount.intValue() == 0) {
			return new MySqlContentDAO();
		}
		return instance;
	}

	public Connection getConnection() throws NamingException, SQLException {

		Context initContext = new InitialContext();
		BasicDataSource envContext = (BasicDataSource) initContext.lookup("java:/comp/env/jdbc/xmusic");
		DataSource ds = (DataSource) envContext;
		return ds.getConnection();
	}

	public void addSong(String Name, String Link, int PricePlanId, int CategoryId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call add_song(?,?,?,?);");
			Statement.setString(1, Name);
			Statement.setString(2, Link);
			Statement.setLong(3, PricePlanId);
			Statement.setLong(4, CategoryId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Add Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Add Song Error during Context Reading!");
		}
	}
	
	@Override
	public void deleteSongCategory(int CategoryId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call remove_song_category(?);");
			Statement.setLong(1, CategoryId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Remove Song Category SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Remove Song Category Error during Context Reading!");
		}
	}

	public void addSongCategory(String Name) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call add_song_category(?);");
			Statement.setString(1, Name);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Add Song Category SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Add Song Category Error during Context Reading!");
		}

	}

	public void deleteSong(int songId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call delete_song(?);");
			Statement.setLong(1, songId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Delete Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Delete Song Error during Context Reading!");
		}
	}

	public void moderateSong(int songId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call moderate_song(?);");
			Statement.setLong(1, songId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Get User's Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Get User's Song Error during Context Reading!");
		}
	}

	public void publishSong(int songId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call publish_song(?);");
			Statement.setLong(1, songId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Publish Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Publish Song Error during Context Reading!");
		}
	}

	public void terminateSong(int songId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call terminate_song(?);");
			Statement.setLong(1, songId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Terminate Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Terminate Song Error during Context Reading!");
		}
	}

	public void unpublishSong(int songId) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call unpublish_song(?);");
			Statement.setLong(1, songId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new ContentDAOException("Unpublish Song SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Unpublish Song Error during Context Reading!");
		}
	}

	public List<Content> getAllContent(String SqlStatement) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(SqlStatement);
			ResultSet resSet = pst.executeQuery();

			List<Content> Songs = convertSongsResultSetToList(resSet);
			connection.close();
			return Songs;
		} catch (SQLException e) {
			throw new ContentDAOException("Get All Content SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Get All Content Error during Context Reading!");
		} catch (ContentCreationException e) {
			throw new ContentDAOException("Get All Content Error during Content Objects Creation!");
		}
	}
	
	@Override
	public Content getSongByName(String Name) throws ContentDAOException {
		try {
			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(this.SqlStatementGetSongByUsername);
			pst.setString(1, Name);
			ResultSet resSet = pst.executeQuery();
			resSet.next();
			
			Content song = ContentFactory.createContent(resSet.getInt("id"), resSet.getString("name"),
					resSet.getString("link"), resSet.getString("price"), resSet.getInt("status_id"),
					resSet.getInt("category_id"));
			connection.close();
			return song;
		} catch (SQLException e) {
			throw new ContentDAOException("Get Song By Name SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Get Song By Name Error during Context Reading!");
		} catch (ContentCreationException e) {
			throw new ContentDAOException("Get Song By Name Error during Content Creation Process!");
		}
	}
	

	@Override
	public List<Category> getCategories() throws ContentDAOException {
		try {
			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(this.SqlStatementGetCategories);
			ResultSet resSet = pst.executeQuery();

			List<Category> Categories = convertCategoryResultSetToList(resSet);
			connection.close();
			return Categories;
		} catch (SQLException e) {
			throw new ContentDAOException("Get Song By Name SQL error!");
		} catch (NamingException e) {
			throw new ContentDAOException("Get Song By Name Error during Context Reading!");
		} catch (ContentCreationException e) {
			throw new ContentDAOException("Get Song By Name Error during Content Creation Process!");
		}
		
	}

	private List<Content> convertSongsResultSetToList(ResultSet resSet) throws ContentCreationException {

		List<Content> Songs = new ArrayList<Content>();
		try {
			while (resSet.next()) {
				Content Song;
				Song = ContentFactory.createContent(resSet.getInt("id"), resSet.getString("name"),
						resSet.getString("link"), resSet.getString("price"), resSet.getInt("status_id"),
						resSet.getInt("category_id"));
				Songs.add(Song);
			}
		} catch (SQLException e) {
			throw new ContentCreationException("SQLException Error during SongsResultSet to List Convertation!");
		}
		return Songs;

	}
	
	private List<Category> convertCategoryResultSetToList(ResultSet resSet) throws ContentCreationException {

		List<Category> Categories = new ArrayList<Category>();
		try {
			while (resSet.next()) {
				Category category = ContentFactory.createCategory(resSet.getInt("id"), resSet.getString("name"), resSet.getString("name_en"));
				Categories.add(category);
			}
		} catch (SQLException e) {
			throw new ContentCreationException("SQLException Error during CategoryResultSet to List Convertation!");
		}
		return Categories;

	}




}
