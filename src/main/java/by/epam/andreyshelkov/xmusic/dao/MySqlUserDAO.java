package by.epam.andreyshelkov.xmusic.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.entities.ContentFactory;
import by.epam.andreyshelkov.xmusic.entities.UserFactory;
import by.epam.andreyshelkov.xmusic.interfaces.UserDAO;
import by.epam.andreyshelkov.xmusic.exceptions.ContentCreationException;
import by.epam.andreyshelkov.xmusic.exceptions.UserCreationException;
import by.epam.andreyshelkov.xmusic.exceptions.UserDAOException;

public class MySqlUserDAO implements UserDAO {

	private static UserDAO instance;
	private static AtomicInteger instanceCount = new AtomicInteger(0);

	private String SqlStatementGetUserContent = "SELECT s.id, s.name, s.link, 0 as price, s.category_id, s.status_id, us.in_playlist FROM xmusic.song s JOIN xmusic.usersong us ON (s.id = us.song_id) WHERE us.user_id = ?";
	private String SqlStatementGetUserByUsername = "SELECT * FROM xmusic.user WHERE username = ? LIMIT 1";
	private String SqlStatementGetUserSongId = "SELECT * FROM xmusic.usersong WHERE user_id = ? AND song_id = ? LIMIT 1";

	private MySqlUserDAO() {
		instanceCount.getAndIncrement();
	}

	public static UserDAO getInstance() {
		if (instanceCount.intValue() == 0) {
			return new MySqlUserDAO();
		}
		return instance;
	}

	public Connection getConnection() throws NamingException, SQLException {
		Context initContext = new InitialContext();
		BasicDataSource envContext = (BasicDataSource) initContext.lookup("java:/comp/env/jdbc/xmusic");
		DataSource ds = (DataSource) envContext;
		return ds.getConnection();
	}

	public void addUser(String Username, String Email, String Phone, String Password, int RoleId) throws UserDAOException {

		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call add_user(?,?,?,?,?);");
			Statement.setString(1, Username);
			Statement.setString(2, Email);
			Statement.setString(3, Phone);
			Statement.setLong(4, RoleId);
			Statement.setString(5, Password);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Add User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Add User Error during Context Reading!");
		}
	}
	
	@Override
	public void deleteUser(int userId) throws UserDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call delete_user(?);");
			Statement.setLong(1, userId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Delete User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Delete User Error during Context Reading!");
		}		
	}

	public void activateUser(int userId) throws UserDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call activate_user(?);");
			Statement.setLong(1, userId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Activate User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Activate User Error during Context Reading!");
		}
	}

	public void deactivateUser(int userId) throws UserDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call deactivate_user(?);");
			Statement.setLong(1, userId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Deactivate User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Deactivate User Error during Context Reading!");
		}
	}

	public void pauseUser(int userId) throws UserDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call pause_user(?);");
			Statement.setLong(1, userId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Pause User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Pause User Error during Context Reading!");
		}
	}

	public void addSongToUser(int userId, int songId) throws UserDAOException {
		try {
			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call add_song_to_user(?,?);");
			Statement.setLong(1, userId);
			Statement.setLong(2, songId);
			Statement.execute();
		} catch (SQLException e) {
			throw new UserDAOException("Get User's SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Get User's Error during Context Reading!");
		}
	}

	public void removeSongFromUser(int userSongId) throws UserDAOException {
		try {

			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call remove_song_from_user(?);");
			Statement.setLong(1, userSongId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Remove Song from User's Profile SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Remove Song from User's Profile Error during Context Reading!");
		}
	}

	public void addSongToPlaylist(int userSongId) throws UserDAOException {
		try {

			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call add_song_to_playlist(?);");
			Statement.setLong(1, userSongId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Add Song to User's Playlist SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Add Song to User's Playlist Error during Context Reading!");
		}
	}

	public void removeSongFromPlaylist(int userSongId) throws UserDAOException {
		try {

			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call remove_song_from_playlist(?);");
			Statement.setLong(1, userSongId);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Remove Song from User's Playlist SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Remove Song from User's Playlist Error during Context Reading!");
		}
	}

	public void changeUser(int userId, String Username, String Email, String Phone, String Password)
			throws UserDAOException {
		try {

			Connection connection = getConnection();
			CallableStatement Statement = connection.prepareCall("call change_user(?,?,?,?,?);");
			Statement.setLong(1, userId);
			Statement.setString(2, Username);
			Statement.setString(3, Email);
			Statement.setString(4, Phone);
			Statement.setString(5, Password);
			Statement.execute();
			connection.close();
		} catch (SQLException e) {
			throw new UserDAOException("Change User SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Change User Error during Context Reading!");
		}
	}

	public List<Content> getUserContent(int userId) throws UserDAOException {
		try {

			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(this.SqlStatementGetUserContent);
			pst.setLong(1, userId);
			ResultSet resSet = pst.executeQuery();

			List<Content> UserContent = convertResultSetToList(resSet);
			connection.close();
			return UserContent;
		} catch (SQLException e) {
			throw new UserDAOException("Get User's Content SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Get User's Content Error during Context Reading!");
		} catch (ContentCreationException e) {
			throw new UserDAOException("Get User's Content Error during Object Creation!");
		}
	}

	private List<Content> convertResultSetToList(ResultSet resSet) throws SQLException, ContentCreationException {
		List<Content> UserContent = new ArrayList<Content>();

		while (resSet.next()) {
			Content song = ContentFactory.createContent(resSet.getInt("id"), resSet.getString("name"), resSet.getString("link"), 
					resSet.getString("price"), resSet.getInt("status_id"), resSet.getInt("category_id"));
			
			Boolean InPlayList = resSet.getBoolean("in_playlist");
			song.setInPlayList(InPlayList);
					
			UserContent.add(song);
		}
		return UserContent;
	}

	public AbstractUser getUserByUsername(String Username) throws UserDAOException {
		try {
			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(this.SqlStatementGetUserByUsername);
			pst.setString(1, Username);
			ResultSet resSet = pst.executeQuery();
			resSet.next();

			AbstractUser user = UserFactory.createSubscriber(resSet.getInt("id"), resSet.getString("username"),
					resSet.getString("email"), resSet.getString("phone"), resSet.getInt("role_id"),
					resSet.getInt("status_id"));
			connection.close();
			return user;
		} catch (SQLException e) {
			throw new UserDAOException("Get User By Name SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Get User By Name Error during Context Reading!");
		} catch (UserCreationException e) {
			throw new UserDAOException("Get User By Name Error during User Creation Process!");
		}
	}

	public int getUserSong(int userId, int songId) throws UserDAOException {
		try {

			Connection connection = getConnection();
			PreparedStatement pst = connection.prepareStatement(this.SqlStatementGetUserSongId);
			pst.setLong(1, userId);
			pst.setLong(2, songId);
			ResultSet resSet = pst.executeQuery();
			resSet.next();
			int userSongId = resSet.getInt("id");
			connection.close();
			return userSongId;
		} catch (SQLException e) {
			throw new UserDAOException("Get User's Song SQL error!");
		} catch (NamingException e) {
			throw new UserDAOException("Get User's Song Error during Context Reading!");
		}
	}


}
