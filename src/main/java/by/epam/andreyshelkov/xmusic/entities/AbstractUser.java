package by.epam.andreyshelkov.xmusic.entities;

import java.io.Serializable;
import java.sql.Date;

public abstract class AbstractUser implements Serializable {
	protected int Id;
	protected String Login;
	protected String Email;
	protected String Phone;
	protected int RoleId;
	protected int StatusId;
	protected Date Created;
	protected Date Updated;
	
	AbstractUser(){}
	
	AbstractUser(int Id, String Username, String Email, String Phone, int RoleId, int StatusId){
		
		this.Id = Id;
		this.Login = Username;
		this.Email = Email;
		this.Phone = Phone;
		this.RoleId = RoleId;
		this.StatusId = StatusId;
	}
	
	public int getId() {
		return Id;
	}

	public String getLogin() {
		return Login;
	}

	public String getEmail() {
		return Email;
	}

	public String getPhone() {
		return Phone;
	}

	public int getRoleId() {
		return RoleId;
	}

	public int getStatusId() {
		return StatusId;
	}

	public Date getCreated() {
		return Created;
	}

	public Date getUpdated() {
		return Updated;
	}
	
}
