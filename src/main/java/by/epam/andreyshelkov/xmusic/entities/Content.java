package by.epam.andreyshelkov.xmusic.entities;

import java.io.Serializable;
import java.util.Date;

public abstract class Content implements Serializable {
	
	protected int Id;
	protected String Name;
	protected String Link;
	protected String Price;
	protected int StatusId;
	protected int CategoryId;
	protected Date Created;
	protected Date Updated;
	protected boolean inPlayList;
	
	Content (int Id, String Name, String Link, String Price, int StatusId, int CategoryId){
		this.Id = Id;
		this.Name = Name;
		this.Link = Link;
		this.Price = Price;
		this.StatusId = StatusId;
		this.CategoryId = CategoryId;
	}

	public int getId() {
		return Id;
	}

	public String getName() {
		return Name;
	}

	public String getLink() {
		return Link;
	}
	
	public String getPrice() {
		return Price;
	}

	public int getStatusId() {
		return StatusId;
	}

	public int getCategoryId() {
		return CategoryId;
	}

	public Date getCreated() {
		return Created;
	}

	public Date getUpdated() {
		return Updated;
	}
	
	public boolean isInPlayList() {
		return inPlayList;
	}

	public void setInPlayList(boolean inPlayList) {
		this.inPlayList = inPlayList;
	}	
	
}
