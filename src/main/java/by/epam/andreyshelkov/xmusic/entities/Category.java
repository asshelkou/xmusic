package by.epam.andreyshelkov.xmusic.entities;

public class Category {
	private int Id;
	private String Name;
	private String NameEn;
	
	Category(int Id, String Name, String NameEn){
		this.Id = Id;
		this.Name = Name;
		this.NameEn = NameEn;
	}
	
	public int getId() {
		return Id;
	}
	public String getName() {
		return Name;
	}
	
	public String getNameEn() {
		return NameEn;
	}
	
}
