package by.epam.andreyshelkov.xmusic.entities;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.exceptions.ContentCreationException;

public class ContentFactory {
	
	private static String ContentImplementationClassName;
	
	static {
		ContentImplementationClassName = PluginConfigurationManager.getPlugin("ContentImplementation");
	}
			
	
	
	public static Content createContent(int Id, String Name, String Link, String Price, int StatusId, int CategoryId) throws ContentCreationException  {
		
		Content content;
		try {
			Class<?> clazz = Class.forName(ContentImplementationClassName);
			Constructor<?> ctor = clazz.getConstructor(Integer.TYPE, String.class, String.class, String.class, Integer.TYPE, Integer.TYPE);
			Object object = ctor.newInstance(Id, Name, Link, Price, StatusId, CategoryId);
			content = (Content) object;
		} catch (ClassNotFoundException e) {
			throw new ContentCreationException("ClassNotFoundException Error during Content Creation Process!");
		} catch (NoSuchMethodException e) {
			throw new ContentCreationException("NoSuchMethodException Error during Content Creation Process!");
		} catch (SecurityException e) {
			throw new ContentCreationException("SecurityException Error during Content Creation Process!");
		} catch (InstantiationException e) {
			throw new ContentCreationException("InstantiationException Error during Content Creation Process!");
		} catch (IllegalAccessException e) {
			throw new ContentCreationException("IllegalAccessException Error during Content Creation Process!");
		} catch (IllegalArgumentException e) {
			throw new ContentCreationException("IllegalArgumentException Error during Content Creation Process!");
		} catch (InvocationTargetException e) {
			throw new ContentCreationException("InvocationTargetException Error during Content Creation Process!");
		}
		if (content == null) {
			throw new ContentCreationException("Cannot Create Content Object!");
		}
		return content;

	}
	
	public static Category createCategory(int Id, String Name, String NameEn) {
		return new Category(Id, Name, NameEn);
	}

}
