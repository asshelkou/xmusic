package by.epam.andreyshelkov.xmusic.entities;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.exceptions.ContentCreationException;
import by.epam.andreyshelkov.xmusic.exceptions.UserCreationException;

public class UserFactory {
	
	private static String UserImplementationClassName;
	private static String SubscriberImplementationClassName;
	
	static {
		UserImplementationClassName = PluginConfigurationManager.getPlugin("UserImplementation");
		SubscriberImplementationClassName = PluginConfigurationManager.getPlugin("SubscriberImplementation");
	}
	public static AbstractUser createUser(int Id, String Username, String Email, String Phone, 
			int RoleId, int StatusId) throws UserCreationException {
		
		AbstractUser user;
		
		try {
			Constructor<?> ctor =  getUserConstructor(UserImplementationClassName);
			Object object = ctor.newInstance(Id, Username, Email, Phone, RoleId, StatusId);
			user = (AbstractUser) object;
		} catch (ClassNotFoundException e) {
			throw new UserCreationException("ClassNotFoundException Error during User Creation Process!");
		} catch (NoSuchMethodException e) {
			throw new UserCreationException("NoSuchMethodException Error during User Creation Process!");
		} catch (SecurityException e) {
			throw new UserCreationException("SecurityException Error during User Creation Process!");
		} catch (InstantiationException e) {
			throw new UserCreationException("InstantiationException Error during User Creation Process!");
		} catch (IllegalAccessException e) {
			throw new UserCreationException("IllegalAccessException Error during User Creation Process!");
		} catch (IllegalArgumentException e) {
			throw new UserCreationException("IllegalArgumentException Error during User Creation Process!");
		} catch (InvocationTargetException e) {
			throw new UserCreationException("InvocationTargetException Error during User Creation Process!");
		}
		
		if (user == null) {
			throw new UserCreationException("Cannot Create User Object!");
		}
		return user;
	}
	
	public static AbstractUser createSubscriber(int Id, String Username, String Email, String Phone, 
			int RoleId, int StatusId) throws UserCreationException {
		
		AbstractUser user;
		
		try {
			Constructor<?> ctor =  getUserConstructor(SubscriberImplementationClassName);
			Object object = ctor.newInstance(Id, Username, Email, Phone, RoleId, StatusId);
			user = (AbstractUser) object;
		} catch (ClassNotFoundException e) {
			throw new UserCreationException("ClassNotFoundException Error during Subscriber Creation Process!");
		} catch (NoSuchMethodException e) {
			throw new UserCreationException("NoSuchMethodException Error during Subscriber Creation Process!");
		} catch (SecurityException e) {
			throw new UserCreationException("SecurityException Error during Subscriber Creation Process!");
		} catch (InstantiationException e) {
			throw new UserCreationException("InstantiationException Error during Subscriber Creation Process!");
		} catch (IllegalAccessException e) {
			throw new UserCreationException("IllegalAccessException Error during Subscriber Creation Process!");
		} catch (IllegalArgumentException e) {
			throw new UserCreationException("IllegalArgumentException Error during Subscriber Creation Process!");
		} catch (InvocationTargetException e) {
			throw new UserCreationException("InvocationTargetException Error during Subscriber Creation Process!");
		}
		
		if (user == null) {
			throw new UserCreationException("Cannot Create Subscriber Object!");
		}
		return user;
	}
	
	private static Constructor<?> getUserConstructor(String ClassName) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
			Class<?> clazz = Class.forName(ClassName);
			Constructor<?> ctor = clazz.getConstructor(Integer.TYPE, String.class, String.class, String.class, Integer.TYPE, Integer.TYPE);
			return ctor;
	}
	

}
