package by.epam.andreyshelkov.xmusic.entities;

import java.util.HashMap;

public class Subscriber extends AbstractUser{

	private HashMap<Integer, Integer> UserSongs = new HashMap<Integer, Integer> ();
	
	public Subscriber(int Id, String Username, String Email, String Phone, int RoleId, int StatusId) {
		super(Id, Username, Email, Phone, RoleId, StatusId);
	}

	public HashMap<Integer, Integer> getUserSongs() {
		return UserSongs;
	}

	public void setUserSongs(HashMap<Integer, Integer> userSongs) {
		UserSongs = userSongs;
	}

}
