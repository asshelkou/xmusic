package by.epam.andreyshelkov.xmusic.jstl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.PublicFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;
import by.epam.andreyshelkov.xmusic.interfaces.PublicFacade;
import by.epam.andreyshelkov.xmusic.specifications.ContentCategorySpecification;
import by.epam.andreyshelkov.xmusic.specifications.ContentStatusSpecification;

public class SongsDisplayer extends TagSupport {

	private static final long serialVersionUID = 1L;
	private static PublicFacade publicFacade;

	private String category;
	private String statuses;

	public SongsDisplayer() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super();
		String PublicFacadeImplementationClassName = PluginConfigurationManager.getPlugin("PublicFacadeImplementation");
		Class<?> clazz = Class.forName(PublicFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		publicFacade = (PublicFacade) object;

	}

	public void setCategory(String value) {
		this.category = value;
	}

	public void setStatuses(String value) {
		this.statuses = value;
	}

	@Override
	public int doStartTag() {
		try {
			List<ContentSpecification> specifications = processAttributes();

			List<Content> songs = publicFacade.getSongs(specifications);
			pageContext.setAttribute("songs", songs);
		} catch (PublicFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	private List<ContentSpecification> processAttributes() {
		List<ContentSpecification> specifications = new ArrayList<ContentSpecification>();

		if (category != null && !category.trim().equals("")) {
			specifications.add(new ContentCategorySpecification(Integer.parseInt(this.category)));
		}

		if (statuses != null && !category.trim().equals("statuses")) {
			specifications.add(new ContentStatusSpecification(statuses));
		}

		return specifications;

	}

}
