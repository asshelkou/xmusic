package by.epam.andreyshelkov.xmusic.jstl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.Category;
import by.epam.andreyshelkov.xmusic.exceptions.PublicFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.PublicFacade;

public class CategoryDisplayer extends TagSupport {

	private static final long serialVersionUID = 1L;
	private PublicFacade publicFacade;

	public CategoryDisplayer() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super();
		String PublicFacadeImplementationClassName = PluginConfigurationManager.getPlugin("PublicFacadeImplementation");
		Class<?> clazz = Class.forName(PublicFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		publicFacade = (PublicFacade) object;
	}

	@Override
	public int doStartTag() {
		try {
			List<Category> Categories = publicFacade.getCategories();
			pageContext.setAttribute("categories", Categories);
		} catch (PublicFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
}
