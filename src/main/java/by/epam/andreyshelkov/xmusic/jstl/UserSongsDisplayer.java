package by.epam.andreyshelkov.xmusic.jstl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;
import by.epam.andreyshelkov.xmusic.interfaces.UserFacade;

public class UserSongsDisplayer extends TagSupport {

	private static final long serialVersionUID = 1L;
	private static UserFacade userFacade;

	private String userId;

	public UserSongsDisplayer() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		super();
		String UserFacadeImplementationClassName = PluginConfigurationManager.getPlugin("UserFacadeImplementation");
		Class<?> clazz = Class.forName(UserFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		userFacade = (UserFacade) object;

	}

	public void setUserId(String value) {
		this.userId = value;
	}

	@Override
	public int doStartTag() {
		try {
			List<Content> songs = userFacade.getContent(Integer.parseInt(userId));
			pageContext.setAttribute("user_songs", songs);
		} catch (UserFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

}
