package by.epam.andreyshelkov.xmusic.facade;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.dao.MySqlUserDAO;
import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.UserDAOException;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;
import by.epam.andreyshelkov.xmusic.interfaces.UserDAO;
import by.epam.andreyshelkov.xmusic.interfaces.UserFacade;
import by.epam.andreyshelkov.xmusic.utils.Hasher;

public class UserFacadeImplementation implements UserFacade {
	
	private static UserFacadeImplementation instance;
	private static AtomicInteger instanceCount = new AtomicInteger(0);
	private UserDAO userDAO = MySqlUserDAO.getInstance();
	
	String UsernameRegexp = "^[A-Za-z]{1}[A-Za-z0-9\\.\\$\\!\\_]+$";
	String EmailRegexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
	String PhoneRegexp = "^\\+375.........$";
	
	private UserFacadeImplementation() {
		instanceCount.getAndIncrement();
	}
	
	public static UserFacade getInstance() throws UserFacadeException {
		if (instanceCount.intValue() == 0) {
			instance = new UserFacadeImplementation();
			return instance;
		}
		return instance;
	}

	public void activate(int UserId) throws UserFacadeException {
		
		try {
			userDAO.activateUser(UserId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("User Activation Error!!!");
		} 
	}

	public void deactivate(int UserId) throws UserFacadeException {
		try {
			userDAO.deactivateUser(UserId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("User Deactivation Error!!!");
		} 
	}

	public void pause(int UserId) throws UserFacadeException {
		try {
			userDAO.pauseUser(UserId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("User Pause error!!!");
		} 
	}

	public void buySong(int UserId, int SongId) throws UserFacadeException {

		try {
			userDAO.addSongToUser(UserId, SongId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Song Adding to User error!!!");
		} 
		
	}

	public void addSongToPlaylist(int UserId, int SongId) throws UserFacadeException {

		try {
			int userSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.addSongToPlaylist(userSongId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Song Adding to User's Playlist error!!!");
		} 	
	}

	public void removeSongFromPlaylist(int UserId, int SongId) throws UserFacadeException {

		try {
			int userSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.removeSongFromPlaylist(userSongId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Song Removing from User's Playlist error!!!");
		} 		
		
	}
	
	@Override
	public void removeSongFromUser(int UserId, int SongId) throws UserFacadeException {
		try {
			int userSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.removeSongFromUser(userSongId);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Song Removing from User's Profile error!!!");
		} 	
		
	}


	public List<Content> getContent(int UserId) throws UserFacadeException {
		
		try {
			List<Content> songs = userDAO.getUserContent(UserId);
			return songs;
		} catch (UserDAOException e) {
			throw new UserFacadeException("Get User Content Error");
		}
		

	}
	
	public AbstractUser getUserDetails(String Username) throws UserFacadeException {
		try {
			return userDAO.getUserByUsername(Username);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Get User's Details error!!!");
		} 	
	}
	
	@Override
	public void registerSubscriber(String Username, String Email, String Phone, String Password, String ConfirmPassword) throws UserFacadeException{
		if (!isUsernameValid(Username)) {
			throw new UserFacadeException("Wrong Username Format");
		}
		
		if (!isEmailValid(Email)) {
			throw new UserFacadeException("Wrong Email Format");			
		}
		
		if (!isPhoneValid(Phone)) {
			throw new UserFacadeException("Wrong Phone Format");			
		}
		
		if (!isPasswordValid(Password, ConfirmPassword)) {
			throw new UserFacadeException("Passwords are not equal Format");			
		}
		
		try {
			userDAO.addUser(Username, Email, Phone, Hasher.calculateHash(Password, PluginConfigurationManager.getPlugin("HashingAlgorithm")), 1);
		} catch (UserDAOException e) {
			throw new UserFacadeException("Error During User Registration");		
		} catch (NoSuchAlgorithmException e) {
			throw new UserFacadeException("Hashing Alghrithm Error User Registration");	
		}
	}

	@Override
	public void recoverSubscriber(String Username, String Email, String Phone, String Password, String ConfirmPassword) throws UserFacadeException{

		AbstractUser User;
		try {
			User = userDAO.getUserByUsername(Username);
		} catch (UserDAOException e1) {
			throw new UserFacadeException("Error User doesn't exist");	
		}
		
		int userId = User.getId();
		
		if (!Email.contentEquals(User.getEmail())) {
			throw new UserFacadeException("Wrong Email for this User");		
		}
		
		if (!Phone.contentEquals(User.getPhone())) {
			throw new UserFacadeException("Wrong Phone for this User");		
		}
		
		if (!isPasswordValid(Password, ConfirmPassword)) {
			throw new UserFacadeException("Passwords are not equal Format");			
		}
		
		try {
			userDAO.changeUser(userId, Username, Email, Phone, Hasher.calculateHash(Password, PluginConfigurationManager.getPlugin("HashingAlgorithm")));
		} catch (UserDAOException e) {
			throw new UserFacadeException("Error During User's Password Recovery");		
		} catch (NoSuchAlgorithmException e) {
			throw new UserFacadeException("Hashing Alghrithm Error Password Recovery");	
		}
		
		
	}
	
	private boolean isUsernameValid(String Username) {
		if (Username.matches(UsernameRegexp)) {
			return true;
		}
		return false;
	}
	
	private boolean isEmailValid(String Email) {
		if (Email.matches(EmailRegexp)) {
			return true;
		}
		return false;
	}
	
	private boolean isPhoneValid(String Phone) {
		if (Phone.matches(PhoneRegexp)) {
			return true;
		}
		return false;
	}
	
	private boolean isPasswordValid(String Password, String ConfirmPassword) {
		if (Password.contentEquals(ConfirmPassword)) {
			return true;
		}
		return false;
	}
	
	public List<Content> getContentList(ContentSpecification specification){
		return new ArrayList<Content>();
	}
	

}
