package by.epam.andreyshelkov.xmusic.facade;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import by.epam.andreyshelkov.xmusic.dao.MySqlContentDAO;
import by.epam.andreyshelkov.xmusic.entities.Category;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.ContentDAOException;
import by.epam.andreyshelkov.xmusic.exceptions.PublicFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.ContentDAO;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;
import by.epam.andreyshelkov.xmusic.interfaces.PublicFacade;

public class PublicFacadeImplementation implements PublicFacade {

	private static PublicFacadeImplementation instance;
	private static AtomicInteger instanceCount = new AtomicInteger(0);
	private ContentDAO songDAO = MySqlContentDAO.getInstance();

	private PublicFacadeImplementation() {
		instanceCount.getAndIncrement();
	}

	public static PublicFacade getInstance() throws PublicFacadeException {
		if (instanceCount.intValue() == 0) {
			instance = new PublicFacadeImplementation();
			return instance;
		}
		return instance;
	}

	@Override
	public List<Content> getSongs(List<ContentSpecification> specifications) throws PublicFacadeException {
		String SqlCondition = "SELECT s.id, s.name, s.link, p.price, s.status_id, s.category_id FROM xmusic.song s JOIN xmusic.priceplan p ON (p.id = s.price_plan_id) WHERE 1=1";

		if (specifications != null) {

			for (ContentSpecification specification : specifications) {
				SqlCondition = SqlCondition + " AND " + specification.getSqlCondition() + " ";
			}
		}
		
		try {
			return songDAO.getAllContent(SqlCondition);
		} catch (ContentDAOException e) {
			throw new PublicFacadeException("Get Songs Error");
		}
	}

	@Override
	public List<Category> getCategories() throws PublicFacadeException {
		try {
			return songDAO.getCategories();
		} catch (ContentDAOException e) {
			throw new PublicFacadeException("Get Categories Error");
		}
	}

}
