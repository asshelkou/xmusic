package by.epam.andreyshelkov.xmusic.facade;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import by.epam.andreyshelkov.xmusic.dao.MySqlContentDAO;
import by.epam.andreyshelkov.xmusic.dao.MySqlUserDAO;
import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.AdminFacadeException;
import by.epam.andreyshelkov.xmusic.exceptions.ContentDAOException;
import by.epam.andreyshelkov.xmusic.exceptions.UserDAOException;
import by.epam.andreyshelkov.xmusic.interfaces.AdminFacade;
import by.epam.andreyshelkov.xmusic.interfaces.ContentDAO;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;
import by.epam.andreyshelkov.xmusic.interfaces.UserDAO;

public class AdminFacadeImplementation implements AdminFacade {
	
	private static AdminFacadeImplementation instance;
	private static AtomicInteger instanceCount = new AtomicInteger(0);
	private UserDAO userDAO = MySqlUserDAO.getInstance();
	private ContentDAO songDAO = MySqlContentDAO.getInstance();
	
	private AdminFacadeImplementation() {
		instanceCount.getAndIncrement();
	}
	
	public static AdminFacade getInstance() throws AdminFacadeException {
		if (instanceCount.intValue() == 0) {
			instance = new AdminFacadeImplementation();
			return instance;
		}
		return instance;
	}

	@Override
	public int addUser(String Username, String Email, String Phone, String Password, String ConfirmPassword, int RoleId)
			throws AdminFacadeException {
		try {
			userDAO.addUser(Username, Email, Phone, ConfirmPassword, RoleId);
			AbstractUser User = userDAO.getUserByUsername(Username);
			return User.getId();
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Creation Error!");
		}
	}

	@Override
	public void changeUser(int UserId, String Username, String Email, String Phone, String Password,
			String ConfirmPassword) throws AdminFacadeException {
		try {
			userDAO.changeUser(UserId, Username, Email, Phone, ConfirmPassword);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Change Error!");
		}
		
	}

	@Override
	public void activateUser(int UserId) throws AdminFacadeException {
		try {
			userDAO.activateUser(UserId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Activation Error!");
		}
		
	}

	@Override
	public void pauseUser(int UserId) throws AdminFacadeException {
		try {
			userDAO.pauseUser(UserId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Pause Error!");
		}
		
	}

	@Override
	public void deactivateUser(int UserId) throws AdminFacadeException {
		try {
			userDAO.deactivateUser(UserId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Deactivation Error!");
		}		
	}

	@Override
	public void deleteUser(int UserId) throws AdminFacadeException {
		try {
			userDAO.deleteUser(UserId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("User Delete Error!");
		}
	}

	@Override
	public void addSongToUser(int UserId, int SongId) throws AdminFacadeException {
		try {
			userDAO.addSongToUser(UserId, SongId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("Add Song to User Error!");
		}	
	}

	@Override
	public void removeSongFromUser(int UserId, int SongId) throws AdminFacadeException {
		try {
			int UserSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.removeSongFromUser(UserSongId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("Remove Song from User Error!");
		}

		
	}

	@Override
	public void addSongToPlaylist(int UserId, int SongId) throws AdminFacadeException {
		try {
			int UserSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.addSongToPlaylist(UserSongId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("Add Song from User's Playlist Error!");
		}
	}

	@Override
	public void removeSongFromPlaylist(int UserId, int SongId) throws AdminFacadeException {
		try {
			int UserSongId = userDAO.getUserSong(UserId, SongId);
			userDAO.removeSongFromPlaylist(UserSongId);
		} catch (UserDAOException e) {
			throw new AdminFacadeException("Add Song from User's Playlist Error!");
		}
	}

	@Override
	public int addSong(String Name, String Link, int PricePlanId, int CategoryId) throws AdminFacadeException {
		try {
			songDAO.addSong(Name, Link, PricePlanId, CategoryId);		
			Content Song = songDAO.getSongByName(Name);
			return Song.getId();	
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Add Song Error!");
		}
	}

	@Override
	public void deleteSong(int SongId) throws AdminFacadeException {
		try {
			songDAO.deleteSong(SongId);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Delete Song Error!");
		}
	}

	@Override
	public void moderateSong(int SongId) throws AdminFacadeException {
		try {
			songDAO.moderateSong(SongId);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Moderate Song Error!");
		}
	}

	@Override
	public void publishSong(int SongId) throws AdminFacadeException {
		try {
			songDAO.publishSong(SongId);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Publish Song Error!");
		}
	}

	@Override
	public void unpublishSong(int SongId) throws AdminFacadeException {
		try {
			songDAO.unpublishSong(SongId);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Unpublish Song Error!");
		}
	}

	@Override
	public void terminateSong(int SongId) throws AdminFacadeException {
		try {
			songDAO.terminateSong(SongId);;
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Unpublish Song Error!");
		}
	}

	@Override
	public void addSongCategory(String CategoryName) throws AdminFacadeException {
		try {
			songDAO.addSongCategory(CategoryName);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Add Song Category Error!");
		}
	}

	@Override
	public void removeSongCategory(int CategoryId) throws AdminFacadeException {
		try {
			songDAO.deleteSongCategory(CategoryId);
		} catch (ContentDAOException e) {
			throw new AdminFacadeException("Delete Song Category Error!");
		}
	}

	@Override
	public List<Content> getContentList(ContentSpecification specification) throws AdminFacadeException {
		// TODO Auto-generated method stub
		return null;
	}

}
