package by.epam.andreyshelkov.xmusic.control;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.exceptions.AdminFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.AdminFacade;

/**
 * Servlet implementation class AdminAction
 */
public class AdminAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AdminFacade adminFacade;
	private String AdminDir = "/xmusic/admin/";
    /**
     * @throws ClassNotFoundException 
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     * @see HttpServlet#HttpServlet()
     */
    public AdminAction() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        super();
		String AdminFacadeImplementationClassName = PluginConfigurationManager.getPlugin("AdminFacadeImplementation");
		Class<?> clazz = Class.forName(AdminFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		adminFacade = (AdminFacade) object;
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			int UserId;
			int SongId;
			String Action = request.getParameter("action");
			AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
			
			switch (Action) {
			case "addUser":
				UserId = adminFacade.addUser(request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
						request.getParameter("password"), request.getParameter("cpassword"), Integer.parseInt(request.getParameter("roleId")));
				request.getSession().setAttribute("message", "User was added successfull");
				request.getServletContext().getRequestDispatcher(AdminDir + "message.jsp?user_id=" + UserId).forward(request, response);
				break;	
			case "changeUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				adminFacade.changeUser(UserId, request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
						request.getParameter("password"), request.getParameter("cpassword"));
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "activateUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				adminFacade.activateUser(UserId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "deactivateUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				adminFacade.deactivateUser(UserId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "pauseUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				adminFacade.pauseUser(UserId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "deleteUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				adminFacade.deleteUser(UserId);
				request.getSession().setAttribute("message", "User was deleted successfull");
				request.getServletContext().getRequestDispatcher(AdminDir + "message.jsp").forward(request, response);
				break;
			case "addSongToUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.addSongToUser(UserId, SongId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "removeSongFromUser":
				UserId = Integer.parseInt(request.getParameter("userId"));
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.removeSongFromUser(UserId, SongId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "addSongToPlaylist":
				UserId = Integer.parseInt(request.getParameter("userId"));
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.addSongToPlaylist(UserId, SongId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "removeSongFromPlaylist":
				UserId = Integer.parseInt(request.getParameter("userId"));
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.removeSongFromPlaylist(UserId, SongId);
				response.sendRedirect(AdminDir + "profile.jsp?user_id=" + UserId);
				break;
			case "addSong":
				SongId = adminFacade.addSong(request.getParameter("name"), request.getParameter("link"),
						Integer.parseInt(request.getParameter("pricaPlanId")), Integer.parseInt(request.getParameter("songCategoryId")));				
				response.sendRedirect(AdminDir + "song.jsp?song_id=" + SongId);
				break;
			case "deleteSong":
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.deleteSong(SongId);
				request.getSession().setAttribute("message", "Song was deleted successfull");
				request.getServletContext().getRequestDispatcher(AdminDir + "message.jsp").forward(request, response);
				break;
			case "moderateSong":
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.moderateSong(SongId);
				response.sendRedirect(AdminDir + "song.jsp?song_id=" + SongId);
				break;
			case "publishSong":
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.publishSong(SongId);
				response.sendRedirect(AdminDir + "song.jsp?song_id=" + SongId);
				break;
			case "unpublishSong":
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.unpublishSong(SongId);
				response.sendRedirect(AdminDir + "song.jsp?song_id=" + SongId);
				break;
			case "terminateSong":
				SongId = Integer.parseInt(request.getParameter("songId"));
				adminFacade.terminateSong(SongId);
				response.sendRedirect(AdminDir + "song.jsp?song_id=" + SongId);
				break;
			case "addSongCategory":
				adminFacade.addSongCategory(request.getParameter("categoryName"));
				response.sendRedirect(AdminDir + "song_categories.jsp");
				break;
			case "removeSongCategory":
				adminFacade.removeSongCategory(Integer.parseInt(request.getParameter("categoryId")));
				response.sendRedirect(AdminDir + "song_categories.jsp");
				break;				
			default:
				response.sendRedirect(AdminDir + "index.jsp");			
			}
			} catch (AdminFacadeException e) {
				response.sendError(500, e.getMessage());
			}
			}

}

