package by.epam.andreyshelkov.xmusic.control;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.UserFacade;

/**
 * Servlet implementation class PublicAction
 */
public class PublicAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(PublicAction.class);
	private UserFacade userFacade;    
    /**
     * @throws ClassNotFoundException 
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     * @see HttpServlet#HttpServlet()
     */
    public PublicAction() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    	super();
		String UserFacadeImplementationClassName = PluginConfigurationManager.getPlugin("UserFacadeImplementation");
		Class<?> clazz = Class.forName(UserFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		userFacade = (UserFacade) object;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
		String Action = request.getParameter("action");
		switch (Action) {
		case "register":
			userFacade.registerSubscriber(request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
					request.getParameter("password"), request.getParameter("cpassword"));
			request.getSession().setAttribute("message", "Your registration was successfull");
			response.sendRedirect("message");
			break;	
		case "recovery":
			userFacade.recoverSubscriber(request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
					request.getParameter("password"), request.getParameter("cpassword"));
			request.getSession().setAttribute("message", "Your password was changed successfully");
			response.sendRedirect("message");
			break;
		default:
			response.sendRedirect("home");
		}
		
		} catch (UserFacadeException e) {
			response.sendError(500, e.getMessage());
		}

	}

}
