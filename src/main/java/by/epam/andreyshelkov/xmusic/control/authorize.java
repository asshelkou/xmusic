package by.epam.andreyshelkov.xmusic.control;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;
import by.epam.andreyshelkov.xmusic.interfaces.UserFacade;

/**
 * Servlet implementation class login
 */
public class authorize extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(authorize.class);
	private UserFacade userFacade;

	/**
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @see HttpServlet#HttpServlet()
	 */
	public authorize() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		super();
		String UserFacadeImplementationClassName = PluginConfigurationManager.getPlugin("UserFacadeImplementation");
		Class<?> clazz = Class.forName(UserFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		userFacade = (UserFacade) object;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			if (request.getUserPrincipal() != null) {
				AbstractUser User = doUser(request);
				request.getSession().setAttribute("user", User);
				response.sendRedirect("subscriber_profile");
				logger.debug("User session was created successfully");
			} else {
				request.getServletContext().getRequestDispatcher("subscriber_profile").forward(request, response);
				logger.debug("Forwarded to User Profile Page");
			}
		} catch (UserFacadeException e) {
			logger.error("Creation User Session Error: " + e.getMessage());
			request.getSession().setAttribute("message", e.getMessage());
			response.sendRedirect("message");
		}
	}

	protected AbstractUser doUser(HttpServletRequest request) throws UserFacadeException {
		Principal userPrincipal = request.getUserPrincipal();
		String Username = userPrincipal.getName();
		logger.debug("doUser() for username - " + Username);
		return userFacade.getUserDetails(Username);
	}

}
