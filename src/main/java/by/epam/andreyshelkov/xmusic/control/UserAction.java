package by.epam.andreyshelkov.xmusic.control;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.andreyshelkov.xmusic.configuration.PluginConfigurationManager;
import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;
import by.epam.andreyshelkov.xmusic.facade.*;
import by.epam.andreyshelkov.xmusic.interfaces.UserFacade;

/**
 * Servlet implementation class UserAction
 */
public class UserAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final Logger logger = LogManager.getLogger(UserAction.class);
	private UserFacade userFacade;

	/**
	 * @throws UserFacadeException
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAction() throws UserFacadeException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super();
		String UserFacadeImplementationClassName = PluginConfigurationManager.getPlugin("UserFacadeImplementation");
		Class<?> clazz = Class.forName(UserFacadeImplementationClassName);
		Method getFacadeInstance = clazz.getMethod("getInstance", null);
		Object object = getFacadeInstance.invoke(clazz, null);
		userFacade = (UserFacade) object;
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
		String Action = request.getParameter("action");
		AbstractUser User = (AbstractUser) request.getSession().getAttribute("user");
		switch (Action) {
		case "Activate":
			userFacade.activate(User.getId());
			updateUserSession(request);
			response.sendRedirect("subscriber_profile");
			break;
		case "Deactivate":
			userFacade.deactivate(User.getId());
			updateUserSession(request);
			response.sendRedirect("subscriber_profile");
			break;
		case "Pause":
			userFacade.pause(User.getId());
			updateUserSession(request);
			response.sendRedirect("subscriber_profile");
			break;
		case "Buy":
			userFacade.buySong(User.getId(), Integer.parseInt(request.getParameter("songId")));
			response.sendRedirect("subscriber_profile");
			break;
		case "Drop":
			userFacade.removeSongFromUser(User.getId(), Integer.parseInt(request.getParameter("songId")));
			response.sendRedirect("subscriber_profile");
			break;
		case "V":
			userFacade.addSongToPlaylist(User.getId(), Integer.parseInt(request.getParameter("songId")));
			response.sendRedirect("subscriber_profile");
			break;
		case "X":
			userFacade.removeSongFromPlaylist(User.getId(), Integer.parseInt(request.getParameter("songId")));
			response.sendRedirect("subscriber_profile");
			break;
		case "getContent":
			userFacade.getContent(User.getId());
			break;
		case "register":
			userFacade.registerSubscriber(request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
					request.getParameter("password"), request.getParameter("cpassword"));
			request.getSession().setAttribute("message", "Your registration was successfull");
			response.sendRedirect("message");
			break;	
		case "recovery":
			userFacade.recoverSubscriber(request.getParameter("username"), request.getParameter("email"), request.getParameter("phone"),
					request.getParameter("password"), request.getParameter("cpassword"));
			request.getSession().setAttribute("message", "Your password was changed successfully");
			response.sendRedirect("message");
			break;
		default:
			response.sendRedirect("home");
		}
		
		} catch (UserFacadeException e) {
			request.getSession().setAttribute("message", e.getMessage());
			response.sendRedirect("message");
		}

	}

	protected void updateUserSession (HttpServletRequest request) throws UserFacadeException {	
		request.getSession().setAttribute("user", doUser(request));
	}
	
	protected AbstractUser doUser(HttpServletRequest request) throws UserFacadeException {
		Principal userPrincipal = request.getUserPrincipal();
		String Username = userPrincipal.getName();
		return userFacade.getUserDetails(Username);
	}
	

}

