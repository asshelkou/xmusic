package by.epam.andreyshelkov.xmusic.specifications;

import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;

public class ContentNameSpecification implements ContentSpecification{

	private String ContentSearchWord;
	private String SqlTemplate;
	
	public ContentNameSpecification(String ContentSearchWord, String SqlTemplate) {
		this.ContentSearchWord = ContentSearchWord.toLowerCase();
		this.SqlTemplate = SqlTemplate;
	}
	
	@Override
	public boolean isSatisfiedBy(Content Song) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSqlCondition() {
		return SqlTemplate.replace("TEMPLATE", ContentSearchWord);
	}

}
