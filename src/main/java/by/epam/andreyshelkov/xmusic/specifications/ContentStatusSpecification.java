package by.epam.andreyshelkov.xmusic.specifications;

import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;

public class ContentStatusSpecification implements ContentSpecification{

	private int[] Statuses;
	
	public ContentStatusSpecification(String Statuses) {
		String[] statuses = Statuses.split(",");
		this.Statuses = new int[statuses.length];
		for (int i = 0; i < statuses.length; i++) {
			this.Statuses[i] = Integer.parseInt(statuses[i]);
		}
	}
	
	@Override
	public boolean isSatisfiedBy(Content Song) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSqlCondition() {
		String SqlCondition = "(";
		int StatusesCount = Statuses.length;
		for (int i = 0; i < StatusesCount; i++) {

			if (i+1 == StatusesCount) {
				SqlCondition = SqlCondition + "status_id = " + Statuses[i] + ")";
			} else {
				SqlCondition = SqlCondition + "status_id = " + Statuses[i] + " OR ";
			}
		
		}
		return SqlCondition;
	}

}
