package by.epam.andreyshelkov.xmusic.specifications;

import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.interfaces.ContentSpecification;

public class ContentCategorySpecification implements ContentSpecification {

	private int CatergoryId;
	
	public ContentCategorySpecification(int CategoryId) {
		this.CatergoryId = CategoryId;
	}
	
	@Override
	public boolean isSatisfiedBy(Content Song) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getSqlCondition() {
		return "category_id=" + this.CatergoryId;
	}

}
