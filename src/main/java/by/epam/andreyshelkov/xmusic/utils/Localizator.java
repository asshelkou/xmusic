package by.epam.andreyshelkov.xmusic.utils;

import java.util.Locale; 
import java.util.ResourceBundle;

public class Localizator {
	private static ResourceBundle bundle;
	
	   public static String execute (String Attribute, String Lang) {
		   Locale locale = new Locale(getLang(Lang));
		   bundle = ResourceBundle.getBundle ("i18n", locale);
           return bundle.getString(Attribute); 
	   }

	   private static String getLang(String Lang) {
		   switch (Lang) {
		   case "en": return "en";
		   case "ru": return "ru";
		   default: return "";
		   }		   
	   }
}
