package by.epam.andreyshelkov.xmusic.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {
	public static String calculateHash(String Text, String HashingAlgorithm) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(HashingAlgorithm);
	    md.update( Text.getBytes( StandardCharsets.UTF_8 ) );
	    byte[] digest = md.digest();
	    String Hex = String.format("%064x", new BigInteger(1,digest));
		return Hex;
	}

}
