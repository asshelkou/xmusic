package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.UserDAOException;

public interface UserDAO {
	public void addUser(String Username, String Email, String Phone, String Password, int RoleId) throws UserDAOException;
	public void deleteUser(int userId) throws UserDAOException;
	public void activateUser(int userId) throws UserDAOException;
	public void deactivateUser(int userId) throws UserDAOException;	
	public void pauseUser(int userId)  throws UserDAOException;	
	public void addSongToUser(int userId, int songId) throws UserDAOException;	
	public void removeSongFromUser(int userSongId) throws UserDAOException;
	public void addSongToPlaylist(int userSongId) throws UserDAOException;	
	public void removeSongFromPlaylist(int userSongId) throws UserDAOException;	
	public void changeUser(int userId, String Username, String Email, String Phone, String Password) throws UserDAOException;
	public List<Content> getUserContent(int userId) throws UserDAOException;
	public AbstractUser getUserByUsername(String Username) throws UserDAOException;
	public int getUserSong(int userId, int songId) throws UserDAOException;
}
