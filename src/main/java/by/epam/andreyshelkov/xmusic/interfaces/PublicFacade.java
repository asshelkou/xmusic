package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.Category;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.PublicFacadeException;

public interface PublicFacade {

	public List<Content> getSongs(List<ContentSpecification> specifications) throws PublicFacadeException ;
	
	public List<Category> getCategories() throws PublicFacadeException ;
}
