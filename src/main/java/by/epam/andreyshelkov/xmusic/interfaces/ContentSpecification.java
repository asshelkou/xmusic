package by.epam.andreyshelkov.xmusic.interfaces;

import by.epam.andreyshelkov.xmusic.entities.Content;

public interface ContentSpecification {
	public boolean isSatisfiedBy(Content Song);
	public String getSqlCondition();
}
