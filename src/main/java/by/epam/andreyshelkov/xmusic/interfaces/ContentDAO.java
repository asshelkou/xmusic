package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.Category;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.ContentDAOException;

public interface ContentDAO {
	public void addSong(String Name, String Link, int PricePlanId, int CategoryId) throws ContentDAOException;
	public void addSongCategory(String Name) throws ContentDAOException;
	public void deleteSongCategory(int CategoryId) throws ContentDAOException;
	public void deleteSong(int songId) throws ContentDAOException;
	public void moderateSong(int songId) throws ContentDAOException;
	public void publishSong(int songId) throws ContentDAOException;
	public void terminateSong(int songId) throws ContentDAOException;
	public void unpublishSong(int songId) throws ContentDAOException;
	public Content getSongByName(String Name) throws ContentDAOException;
	public List<Content> getAllContent(String SqlStatement) throws ContentDAOException;
	public List<Category> getCategories() throws ContentDAOException;
}
