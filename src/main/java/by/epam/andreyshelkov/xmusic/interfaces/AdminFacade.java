package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.AdminFacadeException;

public interface AdminFacade {
	public int addUser(String Username, String Email, String Phone, String Password, String ConfirmPassword, int RoleId) throws AdminFacadeException;
	public void changeUser(int UserId, String Username, String Email, String Phone, String Password, String ConfirmPassword) throws AdminFacadeException;
	public void activateUser(int UserId) throws AdminFacadeException;
	public void pauseUser(int UserId) throws AdminFacadeException;
	public void deactivateUser(int UserId) throws AdminFacadeException;
	public void deleteUser(int UserId) throws AdminFacadeException;
	public void addSongToUser(int UserId, int SongId) throws AdminFacadeException;
	public void removeSongFromUser(int UserId, int SongId) throws AdminFacadeException;
	public void addSongToPlaylist(int UserId, int SongId) throws AdminFacadeException;
	public void removeSongFromPlaylist(int UserId, int SongId) throws AdminFacadeException;	
	
	public int addSong(String Name, String Link, int PricePlanId, int CategoryId) throws AdminFacadeException;
	public void deleteSong(int SongId) throws AdminFacadeException;
	public void moderateSong(int SongId) throws AdminFacadeException;
	public void publishSong(int SongId) throws AdminFacadeException;
	public void unpublishSong(int SongId) throws AdminFacadeException;
	public void terminateSong(int SongId) throws AdminFacadeException;
	public void addSongCategory(String CategoryName) throws AdminFacadeException;
	public void removeSongCategory(int CategoryId) throws AdminFacadeException;
	
	public List<Content> getContentList(ContentSpecification specification) throws AdminFacadeException;
}
