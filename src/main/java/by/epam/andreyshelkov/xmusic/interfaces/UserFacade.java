package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.AbstractUser;
import by.epam.andreyshelkov.xmusic.entities.Content;
import by.epam.andreyshelkov.xmusic.exceptions.UserFacadeException;


public interface UserFacade {
	public void activate(int UserId) throws UserFacadeException;
	public void deactivate(int UserId) throws UserFacadeException ;
	public void pause(int UserId) throws UserFacadeException ;
	public void buySong(int UserId, int SongId) throws UserFacadeException ;
	public void addSongToPlaylist(int UserId, int SongId) throws UserFacadeException ;
	public void removeSongFromPlaylist(int UserId, int SongId) throws UserFacadeException ;
	public void removeSongFromUser(int UserId, int SongId) throws UserFacadeException ;
	public List<Content> getContent(int UserId) throws UserFacadeException ;
	public AbstractUser getUserDetails(String Username) throws UserFacadeException ;
	public void registerSubscriber(String Username, String Email, String Phone, String Password, String ConfirmPassword) throws UserFacadeException ;
	public void recoverSubscriber(String Username, String Email, String Phone, String Password, String ConfirmPassword) throws UserFacadeException ;
	
	public List<Content> getContentList(ContentSpecification specification);
}
