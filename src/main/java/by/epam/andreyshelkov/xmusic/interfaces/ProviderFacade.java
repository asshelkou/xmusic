package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.Content;

public interface ProviderFacade {

	public void addSong(String Name, String Link, int PricePlanId, int CategoryId);
	public void moderateSong(int SongId);
	public void unpublishSong(int SongId);
	public void terminateSong(int SongId);
	
	public List<Content> getContentList(ContentSpecification specification);
	
}
