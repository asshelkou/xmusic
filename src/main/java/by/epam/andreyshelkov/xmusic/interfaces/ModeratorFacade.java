package by.epam.andreyshelkov.xmusic.interfaces;

import java.util.List;

import by.epam.andreyshelkov.xmusic.entities.Content;

public interface ModeratorFacade {
	
	public void publishSong(int SongId);
	public void unpublishSong(int SongId);

	public List<Content> getContentForModeration();
}
