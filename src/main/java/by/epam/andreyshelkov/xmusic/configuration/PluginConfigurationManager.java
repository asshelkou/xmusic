package by.epam.andreyshelkov.xmusic.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PluginConfigurationManager {
	
	static final Logger logger = LogManager.getLogger(PluginConfigurationManager.class);
	
	public static String ConfFileDir;
	
    static
    {
    	ConfFileDir=System.getProperty("XmusicConfigDirectory"); 	
    }
    
    private static Properties plugin_properties;
 
    static
    {
		try {
	    	plugin_properties = new Properties();
	    	logger.debug("Configuration File " + ConfFileDir + "xmusic_plugins.properties");
	    	FileInputStream property_file = new FileInputStream(ConfFileDir + "xmusic_plugins.properties");
			plugin_properties.load(property_file);	
			property_file.close();
		} catch (FileNotFoundException e1) {
			logger.error("Read Configuration File Error: " + e1.getMessage());
		} catch (IOException e) {
			logger.error("Read Configuration File Error: " + e.getMessage());
		}
		
    }
    
    public static String getPlugin(String pluginName) {
    	logger.debug("getPlugein " + pluginName);
    	return plugin_properties.getProperty(pluginName);
    }

}
