package by.epam.andreyshelkov.xmusic.exceptions;

public class AdminFacadeException extends Exception {
	public AdminFacadeException(String message){
		super(message);
	}
}
