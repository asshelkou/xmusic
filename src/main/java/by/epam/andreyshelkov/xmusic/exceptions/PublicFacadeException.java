package by.epam.andreyshelkov.xmusic.exceptions;

public class PublicFacadeException extends Exception {
	public PublicFacadeException(String message){
		super(message);
	}
}
