package by.epam.andreyshelkov.xmusic.exceptions;

public class ContentCreationException extends Exception {
	public ContentCreationException(String message) {
		super(message);
	}
}
