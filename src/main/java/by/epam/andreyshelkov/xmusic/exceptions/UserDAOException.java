package by.epam.andreyshelkov.xmusic.exceptions;

public class UserDAOException extends Exception {
	public UserDAOException(String message) {
		super(message);
	}
}
