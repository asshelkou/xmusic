package by.epam.andreyshelkov.xmusic.exceptions;

public class UserFacadeException extends Exception {
	public UserFacadeException(String message){
		super(message);
	}
}
