package by.epam.andreyshelkov.xmusic.exceptions;

public class ContentDAOException extends Exception {
	public ContentDAOException(String message) {
		super(message);
	}

}
