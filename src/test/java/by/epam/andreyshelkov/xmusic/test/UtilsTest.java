package by.epam.andreyshelkov.xmusic.test;

import java.security.NoSuchAlgorithmException;
import by.epam.andreyshelkov.xmusic.utils.Hasher;
import by.epam.andreyshelkov.xmusic.utils.Localizator;
import junit.framework.Assert;
import junit.framework.TestCase;

public class UtilsTest extends TestCase {
	
	String UsernameRegexp = "^[A-Za-z]{1}[A-Za-z0-9\\.\\$\\!\\_]+$";
	String EmailRegexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
	String PhoneRegexp = "^\\+375.........$";
	
	public void testRegexpMsisdn() {
		String Msisdn = "+375296121857";
		Assert.assertTrue(Msisdn.matches(PhoneRegexp));
	}
	
	public void testRegexpEmail() {
		String Email = "a.shelkov@velcom.by";
		Assert.assertTrue(Email.matches(EmailRegexp));
	}
	
	public void testRegexpUsername() {
		String Username = "a.shelkov";
		Assert.assertTrue(Username.matches(UsernameRegexp));
	}
	
	public void testLocalizatorRu() {
		String expected = "�������";
		String actual = Localizator.execute("header.menu.home", "ru");
		assertEquals(expected, actual);	
	}
	
	public void testLocalizatorEn() {
		String expected = "Home";
		String actual = Localizator.execute("header.menu.home", "en");
		assertEquals(expected, actual);	
		System.out.println((Localizator.execute("form.login.header", "login")));
	}
	
	public void testHasher() throws NoSuchAlgorithmException {
		
		String expected = "2708c3ccaf4bf366d53985264d5ca5cf95d314ede2794a34e81c28ccd50125a9";
		String actual = Hasher.calculateHash("Pas$$W0rd", "SHA-256");

		assertEquals(expected, actual);		
	}
	
}
